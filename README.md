# Solar System #

A simple animated solar system.

### What is this repository for? ###

* To have fun

---
This code is not up-to-date and therefore does not use requestAnimationFrame. All animations are done in a simple SetTimeout loop. Obviously, this is not the best way to do this.

## TODO ##

* Implement requestAnimationFrame for animations instead of SetTimeout
* Add gravity to really mess with planets and moons :)
* Project moon explosions in psuedo-realistic direction