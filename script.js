var starX;
var starY;
var radius = 30;
var v = 50;
var planets = [];
var moons = [];
var angle;
var newX;
var newY;
var st;
var gameOver = false;
var collision = -1;
var el = document.getElementById('debug');

function init() {
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');
    canvas2 = document.getElementById('canvas2');
    ctx2 = canvas2.getContext('2d');
    clear_canvas();
    starX = canvas.width / 2;
    starY = canvas.height / 2;
    planets = [];
    moons = [];
    el.innerHTML = "";
    create_planets(Math.ceil(Math.random()*3+3));
    frameRate = 30;
    frameTimer = 1000 / frameRate;

    game_loop();
}

function game_loop() {
    clear_canvas();
    draw_star();
    move_planets();
    draw_planets();
    move_moons();
    draw_moons();

    moon_collision();
    planet_collision();

    if (!gameOver) {
        game = setTimeout("game_loop()", frameTimer);
    }
}

function clear_canvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function draw_star() {
    ctx.fillStyle = "rgba(255, 255, 0, 0.1)";
    ctx.beginPath();
    ctx.arc(starX, starY, radius, 0, Math.PI*2, false);
    ctx.fill();
    ctx.closePath();

    // create radial gradient
    var grd = ctx.createRadialGradient(starX, starY, 5, starX, starY, radius);
    grd.addColorStop(0, "#fff");
    grd.addColorStop(.6, "#dd0");
    grd.addColorStop(.8, "rgba(255, 160, 0, 0.5)");
    grd.addColorStop(1, "rgba(255, 255, 255, 0.05)");
    ctx.fillStyle = grd;
    ctx.fill();
}

function create_planets(num) {
    var limit = Math.ceil((canvas.height / 2) / (num + 1));
    for (var i=0; i<num; i++) {
        pRadius = Math.ceil(Math.random() * 10) + ((i+2) * 2);
        var rand = Math.ceil(Math.random() * limit);
        var magnitude = rand + pRadius;
        if (i > 0) {
            magnitude = magnitude + planets[i-1].radius + planets[i-1].magnitude;
        }
        if (magnitude < (radius + pRadius + 60)) magnitude = radius + pRadius + 60;
        var ang = Math.random() * 2 * Math.PI;
        var nX = Math.cos(ang) * magnitude + starX;
        var nY = Math.sin(ang) * magnitude + starY;
        var speed = deg2rad(Math.random() / 2 + .1);
        var r = (Math.ceil(Math.random() * 7) + 2);
        var g = (Math.ceil(Math.random() * 7) + 2);
        var b = (Math.ceil(Math.random() * 7) + 2);
        var color = "#" + String(r) + String(g) + String(b);
        planets.push({
            magnitude: magnitude,
            angle: ang,
            x: nX,
            y: nY,
            radius: pRadius,
            speed: speed,
            color: color
        });
        ctx.beginPath();
        ctx.fillStyle = "#009";
        ctx.arc(nX, nY, pRadius, 0, Math.PI*2, false);
        ctx.fill();
        //if (Math.random() > .1) create_moon(planets[i], i);
        for (var n=0; n<Math.random()*3; n++) {
            create_moon(planets[i], i);
        }
        //el.innerHTML += "magnitude: "+magnitude+", limit:"+limit+", tmp: "+tmp+", radius: "+radius+"<br />";
    }
}

function move_planets() {
    for (var i = 0; i<planets.length; i++) {
        var thisPlanet = planets[i];
        thisPlanet.angle += thisPlanet.speed;
        if (thisPlanet.angle > Math.PI * 2) planets[i].angle = 0;
        thisPlanet.x = Math.cos(thisPlanet.angle) * thisPlanet.magnitude + starX;
        thisPlanet.y = Math.sin(thisPlanet.angle) * thisPlanet.magnitude + starY;
        moons.px = thisPlanet.x;
        moons.py = thisPlanet.y;
    }
}

function draw_planets() {
    ctx.fillStyle = "#009";

    for (var i = 0; i<planets.length; i++) {
        // draw orbital path
        ctx.strokeStyle = "rgba(48, 48, 48, 1)";
        var thisPlanet = planets[i];
        ctx.beginPath();
        ctx.arc(starX, starY, thisPlanet.magnitude, 0, Math.PI*2, false);
        ctx.stroke();
        ctx.closePath();

        // draw planet
        ctx.beginPath();
        ctx.arc(thisPlanet.x, thisPlanet.y, thisPlanet.radius, 0, Math.PI*2, false);
        ctx.fill();
        ctx.closePath();
        ctx.fillStyle = "#fff";
        ctx.font = '10px sans-serif';
        ctx.fillText('Angle: '+Math.ceil(rad2deg(thisPlanet.angle)), thisPlanet.x + thisPlanet.radius, thisPlanet.y - thisPlanet.radius);
        ctx.fillText('Distance: '+thisPlanet.magnitude, thisPlanet.x + thisPlanet.radius, thisPlanet.y - thisPlanet.radius+10);

        // create radial gradient
        var gX = Math.cos(thisPlanet.angle) * (thisPlanet.magnitude - (thisPlanet.radius)) + starX;
        var gY = Math.sin(thisPlanet.angle) * (thisPlanet.magnitude - (thisPlanet.radius)) + starY;
        var grd = ctx.createRadialGradient(gX, gY, thisPlanet.radius / 4, gX, gY, thisPlanet.radius*2);
        grd.addColorStop(0, "#ddd");
        grd.addColorStop(0.7, thisPlanet.color);
        grd.addColorStop(1, "#000000");
        ctx.fillStyle = grd;
        ctx.fill();
    }
}

function create_moon(p, i) {
    mRadius = Math.ceil(Math.random() * (p.radius/3)) + 2;
    //var magnitude = mRadius + pRadius + 15;
    var magnitude = mRadius + pRadius + Math.random() * 10 + 12;
    var ang = Math.random() * 2 * Math.PI;
    var mX = Math.cos(ang) * magnitude + p.x;
    var mY = Math.sin(ang) * magnitude + p.y;
    //var speed = deg2rad(Math.random() / 2 + .6);
    var speed = planets[i].speed;
    var gray = (Math.ceil(Math.random() * 7) + 2);
    var color = "#" + String(gray) + String(gray) + String(gray);
    moons.push({
        magnitude: magnitude,
        angle: ang,
        x: mX,
        y: mY,
        px: p.x,
        py: p.y,
        radius: mRadius,
        speed: speed,
        color: color,
        planet: i
    });
    ctx.beginPath();
    ctx.fillStyle = '#'+color+color+color;
    ctx.arc(mX, mY, mRadius, 0, Math.PI*2, false);
    ctx.fill();
}

function draw_moons() {
    for (var i = 0; i<moons.length; i++) {
        var m = moons[i];
        var p = planets[m.planet];
        // draw moon
        ctx.beginPath();
        ctx.arc(m.x, m.y, m.radius, 0, Math.PI*2, false);
        ctx.fill();
        ctx.closePath();

        //ctx.fillStyle = "#fff";
        //ctx.font = '10px sans-serif';
        //ctx.fillText('Moon '+i, m.x + m.radius, m.y - m.radius);

        // create radial gradient
        var gX = Math.sin(m.angle) * (m.magnitude - (m.radius)) + p.x;
        var gY = Math.cos(m.angle) * (m.magnitude - (m.radius)) + p.y;
        var grd = ctx.createRadialGradient(gX, gY, m.radius / 4, gX, gY, m.radius*2);
        grd.addColorStop(0, "#ddd");
        grd.addColorStop(0.7, m.color);
        grd.addColorStop(1, "#000000");
        ctx.fillStyle = grd;
        ctx.fill();
    }
}

function move_moons() {
    for (var i = 0; i<moons.length; i++) {
        var m = moons[i];
        var p = planets[m.planet];
        m.angle += m.speed;
        m.x = Math.sin(m.angle) * m.magnitude + p.x;
        m.y = Math.cos(m.angle) * m.magnitude + p.y;
    }
}

function deg2rad(deg) {
    return Math.PI / 180 * deg;
}

function rad2deg(rad) {
    return 180 / Math.PI * rad;
}

function moon_collision() {
    var pj, mi, mj;
    if (moons.length > 1) {
        for (var i = 0; i<moons.length-1; i++) {
            for (var j = 1; j<moons.length; j++) {
                if (i != j) {
                    mi = moons[i];
                    mj = moons[j];
                    var xdist = Math.abs(mi.x - mj.x);
                    var ydist = Math.abs(mi.y - mj.y);
                    var dist = Math.sqrt(Math.pow(xdist, 2) + Math.pow(ydist, 2));
                    if (dist < (mi.radius + mj.radius)) {
                        el.innerHTML += "COLLISION: Moon "+i+" with Moon "+j+"<br />";
                        // smaller body dies
                        if (mi.radius > mj.radius) {
                            collision = j;
                        } else {
                            collision = i;
                        }
                        var emitParticles = emit(moons[collision].x, moons[collision].y);
                        emitParticles.start();
                    }
                }
            }
            if (collision >= 0) {
                moons.splice(collision, 1);
                collision = -1;
                break;
            }

        }
    }
}

function planet_collision() {
    for (var i = 0; i<moons.length; i++) {
        for (var j = 0; j<planets.length; j++) {
            xdist = Math.abs(moons[i].x - planets[j].x);
            ydist = Math.abs(moons[i].y - planets[j].y);

            var dist = Math.sqrt(Math.pow(xdist, 2) + Math.pow(ydist, 2));
            if (dist < moons[i].radius + planets[j].radius) {
                el.innerHTML += "COLLISION: Moon "+i+" with Planet "+j+"<br />";
                collision = i;
                var emitParticles = emit(moons[i].x, moons[i].y);
                emitParticles.start();
                break;
            }

        }
        if (collision >= 0) {
            moons.splice(collision, 1);
            collision = -1;
            return;
        }
    }
}

function emit(mx, my)
{
    var NUM_POINTS = 25;

    var points = [],
        width = canvas.width,
        height = canvas.height,
        interval;

    //var gravity = .4;
    var gravity = 0;

    //var emitter = { x: randBtwn(0, width), y: randBtwn(0, height) };
    //var emitter = { x: width/2, y: height/2 };
    var emitter = { x: mx, y: my };


    function randBtwn(min, max)
    {
        return min + (max - min) * Math.random();
    }

    function resetPoint(p)
    {
        p.x  = emitter.x + randBtwn(-10, 10);
        p.y  = emitter.y + randBtwn(-10, 5);
        p.vY = randBtwn(-12, 3);
        p.vX = randBtwn(-9, 9);
        p.radius = randBtwn(1, 3);
    }

    function initPoint()
    {
        var p = { };
        resetPoint(p);
        return p;
    }

    function addPoint()
    {
        //if(points.length < NUM_POINTS)
            //points.push(initPoint());
        for (var i=0; i<NUM_POINTS; i++) {
            points.push(initPoint());
        }
    }

    var circRadius = 4;//pix
    function draw()
    {
        ctx2.clearRect(0, 0, width, height);
        var point, i;
        for(i = 0; i < points.length; i++)
        {
            point = points[i];
            ctx2.beginPath(point.x, point.y);
            //ctx2.fillStyle = 'rgba(255,255,255, '+ emitter.y/point.y - 1  +')';
            ctx2.fillStyle = 'rgba(255,255,255,1)';
            //context.arc(point.x, point.y, point.radius, 0, 2*Math.PI);
            ctx2.rect(point.x, point.y, point.radius, point.radius);
            ctx2.fill();
        }
    }

    function move(t)
    {
        var i = 0;
        points.forEach(function(point) {
            if((point.y > (height - point.radius) || point.y < point.radius)
                ||
                (point.x > (width - point.radius) || point.x < point.radius)) {
                    points.splice(i, 1);
                    //resetPoint(point);
            } else {
                point.vY += gravity;
                point.x += point.vX;
                point.y += point.vY;
            }
            i++;
        });
        //el.innerHTML = "Points = " + points.length;
    }
    return {
        'start': function() {
            var start = new Date();

            points = [];
            addPoint();
            window.clearInterval(interval);
            interval = window.setInterval(function() {
                //addPoint();
                move(new Date() - start);
                draw();
            }, 1000/30);
        },
        'stop': function() {
            window.clearInterval(interval);
        }
    };

}

init();
